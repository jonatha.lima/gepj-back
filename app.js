module.exports = function(app) {
    app.listen(app.get('port'), () => {
        console.log('Server running', app.get('port'));
    })
}