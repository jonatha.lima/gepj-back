"use strict";

var express = require('express');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var cors = require('cors');
var expressValidator = require('express-validator');

module.exports = function(app) {
    app.set('port', process.env.PORT);
    app.set('json spaces', 4);
    
    app.use(helmet());
    app.use(cors());
    app.use(bodyParser.json({limit: '5mb'}));
    app.use(bodyParser.urlencoded({
        limit: '5mb',
        extended: true,
        parameterLimit: 10000000
    }));
    app.use(expressValidator());
    app.use(methodOverride());
    app.use(methodOverride('X-HTTP-Method'));
    app.use(methodOverride('X-HTTP-Method-Override'));
    app.use(methodOverride('X-Method-Override'));
    app.use(methodOverride('_method')); // action="/point?_method=PUT"
}
